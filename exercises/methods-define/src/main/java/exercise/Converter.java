package exercise;

class Converter {
    // BEGIN
    public static int convert(int number, String requestedUnit) {
        if (requestedUnit.equals("b")) {
            return number * 1024;
        } else if (requestedUnit.equals("Kb")) {
            return number / 1024;
        } else {
            return 0;
        }
    }

    public static void main(String[] args) {
        int numberOfKb = 10;
        int numberOfBytes = Converter.convert(numberOfKb, "b");

        System.out.println(numberOfKb + " Kb = " + numberOfBytes + " b");
    }
    // END
}
