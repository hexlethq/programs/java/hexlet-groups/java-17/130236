package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(int firstSide, int secondSide, int angle) {
        double sineAngle = Math.sin((angle * Math.PI) / 180);
        double square =  0.5 * firstSide * secondSide * sineAngle;

        return square;
    }

    public static void main(String[] args) {
        System.out.println(Triangle.getSquare(4, 5, 45));
    }
    // END
}
