package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String openNumbers = cardNumber.substring(12);

        for (int i = 0; i < starsCount; i += 1) {
            System.out.print("*");
        }

        System.out.println(openNumbers);

        // END
    }
}
