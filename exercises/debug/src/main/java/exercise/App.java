package exercise;

class App {
    // BEGIN
    public static boolean doesTriangleExist(int firstSide, int secondSide, int thirdSide) {
        if ((firstSide + secondSide) > thirdSide
                && (firstSide + thirdSide) > secondSide
                && (secondSide + thirdSide) > firstSide) {
            return true;
        }
        return false;
    }

    public static String getTypeOfTriangle(int firstSide, int secondSide, int thirdSide) {
        if (!(doesTriangleExist(firstSide, secondSide, thirdSide))) {
            return "Треугольник не существует";
        } else if (firstSide == secondSide && firstSide == thirdSide) {
            return "Равносторонний";
        } else if (firstSide != secondSide && firstSide != thirdSide && secondSide != thirdSide) {
            return "Разносторонний";
        } else {
            return "Равнобедренный";
        }
    }
}
        // END
